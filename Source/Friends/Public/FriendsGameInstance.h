// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "FriendsGameInstance.generated.h"

/**
 *
 */
UCLASS()
class FRIENDS_API UFriendsGameInstance : public UGameInstance {
	GENERATED_BODY()

public:
	UFriendsGameInstance();

protected:
	virtual void Init() override;
};
