// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Online.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Friends/Public/SteamFriendStruct.h"

#include "FriendUser.generated.h"

/**
 *
 */

class UFriendsList;

DECLARE_DELEGATE_OneParam(InviteDelegate, UFriendUser*)
DECLARE_DELEGATE_TwoParams(CancelDelegate, UFriendUser*, bool)

UCLASS(BlueprintType, Blueprintable)
class FRIENDS_API UFriendUser : public UUserWidget {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString name;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ESlateVisibility buttonVisibility;

	UFUNCTION(BlueprintCallable)
		void invite();

	UFUNCTION(BlueprintCallable)
		void cancel();

	bool checkIfOnline();
	void set(const FSteamFriendStruct& steamFriend, UFriendsList* friendsList);
private:
	IOnlineFriendsPtr onlineFriends;
	FSteamFriendStruct user;

	void onSendInviteComplete(int32 localUserNum, bool isSuccess, const FUniqueNetId& uniqueNetId, const FString& listName, const FString& error);

	InviteDelegate inviteDelegate;
	CancelDelegate cancelDelegate;
};
