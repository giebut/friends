// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Friends/Public/SteamFriendStruct.h"

#include "MockDataAsset.generated.h"

/**
 *
 */
UCLASS(Blueprintable)
class FRIENDS_API UMockDataAsset : public UDataAsset {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FSteamFriendStruct> steamFriends;
};
