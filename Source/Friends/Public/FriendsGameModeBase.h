// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FriendsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class FRIENDS_API AFriendsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
