// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "FriendsPlayerController.generated.h"

/**
 *
 */
UCLASS()
class FRIENDS_API AFriendsPlayerController : public APlayerController {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UUserWidget> menu;

	virtual void BeginPlay() override;

private:
	UUserWidget* menuInstance;
};
