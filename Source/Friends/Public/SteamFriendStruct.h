// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SteamFriendStruct.generated.h"

USTRUCT(BlueprintType)
struct FSteamFriendStruct {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool isLogged;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FString name;

	TSharedPtr<const FUniqueNetId> uniqueNetId;

	FSteamFriendStruct();
};