// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Online.h"
#include "Blueprint/UserWidget.h"
#include "Components/VerticalBox.h"
#include "Friends/Public/MockDataAsset.h"

#include "FriendsList.generated.h"

/**
 *
 */

class UFriendUser;

UCLASS(BlueprintType, Blueprintable)
class FRIENDS_API UFriendsList : public UUserWidget {
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UUserWidget> userTemplate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UMockDataAsset> mockData;

	UFUNCTION(BlueprintCallable)
		void generateUserWidgets(UVerticalBox* inviteVerticalBox, UVerticalBox* onlineVerticalBox, UVerticalBox* offlineVerticalBox);

	UFriendsList(const FObjectInitializer&);

	IOnlineFriendsPtr getFriendsInterface();
	void invite(UFriendUser* friendUser);
	void cancel(UFriendUser* friendUser, bool isLogged);

	virtual void NativeTick(const FGeometry&, float) override;
private:
	IOnlineFriendsPtr onlineFriends;
	UVerticalBox* inviteList;
	UVerticalBox* onlineList;
	UVerticalBox* offlineList;

	UFriendUser* createFriendUser();

	void addToList(UFriendUser*friendUser, bool isLogged);
	void generateSteamUserWidgets();
	void generateMockUserWidgets();

	void onReadComplete(int32 localUserNum, bool isSuccess, const FString& listName, const FString& error);
};
