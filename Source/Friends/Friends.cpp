// Copyright Epic Games, Inc. All Rights Reserved.

#include "Friends.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Friends, "Friends" );
