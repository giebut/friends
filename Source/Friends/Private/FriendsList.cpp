// Fill out your copyright notice in the Description page of Project Settings.

#include "Friends/Public/FriendUser.h"

#include "Friends/Public/FriendsList.h"

UFriendsList::UFriendsList(const FObjectInitializer&objectInitializer) : UUserWidget(objectInitializer) {}

void UFriendsList::NativeTick(const FGeometry& geometry, float deltaTime) {
	Super::NativeTick(geometry, deltaTime);
}

void UFriendsList::generateUserWidgets(UVerticalBox*inviteVerticalBox, UVerticalBox*onlineVerticalBox, UVerticalBox*offlineVerticalBox) {
	inviteList = inviteVerticalBox;
	onlineList = onlineVerticalBox;
	offlineList = offlineVerticalBox;

#if WITH_EDITOR
	if(GIsEditor) {
		generateMockUserWidgets();

		return;
	}
#endif
	generateSteamUserWidgets();
}

IOnlineFriendsPtr UFriendsList::getFriendsInterface() {
	return onlineFriends;
}

void UFriendsList::invite(UFriendUser* friendUser) {
	onlineList->RemoveChild(friendUser);
	inviteList->AddChild(friendUser);
}

void UFriendsList::cancel(UFriendUser* friendUser, bool isLogged) {
	inviteList->RemoveChild(friendUser);

	if(isLogged) {
		onlineList->AddChild(friendUser);
	} else {
		offlineList->AddChild(friendUser);
	}
}

UFriendUser* UFriendsList::createFriendUser() {
	return userTemplate == nullptr ? nullptr : Cast<UFriendUser>(CreateWidget<UUserWidget>(this, userTemplate));
}

void UFriendsList::addToList(UFriendUser*friendUser, bool isLogged) {
	if(isLogged) {
		if(onlineList != nullptr) {
			onlineList->AddChild(friendUser);
		}
	} else {
		if(offlineList != nullptr) {
			offlineList->AddChild(friendUser);
		}
	}
}

void UFriendsList::generateSteamUserWidgets() {
	auto onlineSubsystem = IOnlineSubsystem::Get();

	onlineFriends = onlineSubsystem->GetFriendsInterface();

	bool isRead = onlineFriends != nullptr && onlineFriends->ReadFriendsList(
		0,
		EFriendsLists::ToString(EFriendsLists::Default),
		FOnReadFriendsListComplete::CreateUObject(this, &UFriendsList::onReadComplete)
	);
}

void UFriendsList::generateMockUserWidgets() {
	auto data = mockData->GetDefaultObject<UMockDataAsset>();

	if(data != nullptr) {
		for(int i = 0; i < data->steamFriends.Num(); i++) {
			auto friendUser = createFriendUser();

			friendUser->set(data->steamFriends[i], this);

			addToList(friendUser, data->steamFriends[i].isLogged);
		}
	}
}

void UFriendsList::onReadComplete(int32 localUserNum, bool isSuccess, const FString& listName, const FString& error) {
	TArray<TSharedRef<FOnlineFriend>> friends;

	bool isGet = isSuccess && onlineFriends->GetFriendsList(localUserNum, EFriendsLists::ToString(EFriendsLists::Default), friends);

	if(isGet) {
		for(int i = 0; i < friends.Num(); i++) {
			auto friendUser = createFriendUser();

			FSteamFriendStruct steamFriend;

			steamFriend.name = friends[i]->GetDisplayName();
			steamFriend.isLogged = friends[i]->GetPresence().bIsOnline;
			steamFriend.uniqueNetId = friends[i]->GetUserId();

			friendUser->set(steamFriend, this);

			addToList(friendUser, steamFriend.isLogged);
		}
	}
}