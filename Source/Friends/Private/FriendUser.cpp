// Fill out your copyright notice in the Description page of Project Settings.

#include "Friends/Public/FriendsList.h"

#include "Friends/Public/FriendUser.h"

void UFriendUser::invite() {
#if WITH_EDITOR	
	if(GIsEditor) {
		buttonVisibility = ESlateVisibility::Collapsed;
		inviteDelegate.ExecuteIfBound(this);
	}

	return;
#endif
	onlineFriends->SendInvite(
		0,
		*user.uniqueNetId,
		EFriendsLists::ToString(EFriendsLists::Default),
		FOnSendInviteComplete::CreateUObject(this, &UFriendUser::onSendInviteComplete)
	);
}

void UFriendUser::cancel() {
#if WITH_EDITOR	
	if(GIsEditor) {
		buttonVisibility = user.isLogged ? ESlateVisibility::Visible : ESlateVisibility::Collapsed;
		cancelDelegate.ExecuteIfBound(this, user.isLogged);
	}

	return;
#endif
	auto onlineFriend = onlineFriends->GetFriend(0, *user.uniqueNetId, EFriendsLists::ToString(EFriendsLists::Default));

	user.isLogged = onlineFriend->GetPresence().bIsOnline;

	buttonVisibility = user.isLogged ? ESlateVisibility::Visible : ESlateVisibility::Collapsed;
	cancelDelegate.ExecuteIfBound(this, user.isLogged);
}

void UFriendUser::set(const FSteamFriendStruct& steamFriend, UFriendsList* friendsList) {
	user = steamFriend;
	name = user.name;
	buttonVisibility = user.isLogged ? ESlateVisibility::Visible : ESlateVisibility::Collapsed;

	onlineFriends = friendsList->getFriendsInterface();

	inviteDelegate.BindUObject(friendsList, &UFriendsList::invite);
	cancelDelegate.BindUObject(friendsList, &UFriendsList::cancel);
}

void UFriendUser::onSendInviteComplete(int32 localUserNum, bool isSuccess, const FUniqueNetId& uniqueNetId, const FString& listName, const FString& error) {
	buttonVisibility = ESlateVisibility::Collapsed;
	inviteDelegate.ExecuteIfBound(this);
}