// Fill out your copyright notice in the Description page of Project Settings.

#include "Blueprint/UserWidget.h"

#include "Friends/Public/FriendsPlayerController.h"

void AFriendsPlayerController::BeginPlay() {
	Super::BeginPlay();

	if(menu!=nullptr)	{
		menuInstance = CreateWidget(this, menu);

		if(menuInstance)		{
			menuInstance->AddToViewport();

			SetInputMode(FInputModeUIOnly());
		}

		bShowMouseCursor = true;
	}
}